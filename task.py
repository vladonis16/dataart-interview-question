import pymongo
import pandas as pd

db_client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
db = db_client["dataart_interview_question"]
collection = db["jobs"]

# Getting db entries with statuses "submitted" and "running" only
df = pd.DataFrame(
    list(
        collection.aggregate(
            [
                {"$match": {"$or": [{"status": "submitted"}, {"status": "running"}]}},
                {"$sort": {"record_ts": 1}},
            ],
            allowDiskUse=True,
        )
    )
)

# Grouping dataframe by 1H interval and getting number of unique job_ids in that intervals
ndf = (
    df.groupby([pd.Grouper(key="record_ts", freq="1H")])["job_id"]
    .nunique()
    .reset_index()
)

# Sorting dataframe descending
ndf = ndf.sort_values("job_id", ascending=False)

# Printing result to console
print(ndf.head())
