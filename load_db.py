import datetime
import random
import pymongo

TASK_FINAL_STATUSES = ["failed", "successful"]

TASKS_COUNT = 1600000
DAYS_FROM_NOW = 365


def get_random_datetime(start_datetime, end_datetime):
    time_between_dates = end_datetime - start_datetime
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange(days_between_dates)
    random_number_of_seconds = random.randrange(86400)
    random_datetime = start_datetime + datetime.timedelta(
        days=random_number_of_days, seconds=random_number_of_seconds
    )
    return random_datetime


def add_random_timedelta(from_datetime, minutes_range):
    random_minutes = datetime.timedelta(minutes=random.randrange(minutes_range))
    return from_datetime + random_minutes


def seed_values():
    db_client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
    db = db_client["dataart_interview_question"]
    collection = db["jobs"]
    end_datetime = datetime.datetime.utcnow()
    start_datetime = end_datetime - datetime.timedelta(days=DAYS_FROM_NOW)
    entries = []
    for i in range(1, TASKS_COUNT + 1):
        task_submit_ts = get_random_datetime(start_datetime, end_datetime)
        task_running_ts = add_random_timedelta(task_submit_ts, 10)
        task_final_status_ts = add_random_timedelta(task_running_ts, 10)

        entries.extend(
            [
                {"job_id": i, "status": "submitted", "record_ts": task_submit_ts},
                {"job_id": i, "status": "running", "record_ts": task_running_ts},
                {
                    "job_id": i,
                    "status": random.choice(TASK_FINAL_STATUSES),
                    "record_ts": task_final_status_ts,
                },
            ]
        )
    collection.insert_many(entries)

    return


if __name__ == "__main__":
    seed_values()
